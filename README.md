# Gutho Discord Proxy

This proxy acts as a bridge between the hangman bot and Discord.

## Development

To develop this application, you must have the following dependencies installed on your development environment.

### 1. Go Programming Language

Refer to this [page](https://golang.org/doc/install) to install Go programming language on your computer.

### 2. Go Dependencies

After you installed Go programming language in your computer, you have to get the dependencies of this project. To do that, you can run the command ```go get -d ./...```.

### 3. Environment Variables

To run this project, you will need to setup these following environment variables.

| Variable Name      | Function                      |
|-------------------:|-------------------------------|
| **GUTHO_TOKEN**    | Discord bot's Token           |
| **GUTHO_MAIN_URL** | Gutho backend service's URL   |
| **PORT**           | Port for this service         |

### 4. Development Server

To test the program, you can use the command ```go run *.go``` to start a the program. You can also use the command ```docker-compose up``` to start a Docker container running the program's image assuming you have Docker installed in your computer.

### 5. Build

This program is Docker ready. You can use Docker Compose in your production server to start the program.

## Author

This program was made by <br>
Samuel (1906285592) <br>
samuel92(at)ui.ac.id
