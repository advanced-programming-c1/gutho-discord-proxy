package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/bwmarrin/discordgo"
	"github.com/gin-gonic/gin"
)

func generateRequestBody(message *discordgo.MessageCreate) []byte {
	requestBody, Err := json.Marshal(gin.H{
		"channelID": message.ChannelID,
		"userID":    message.Author.ID,
		"message":   message.Content,
	})
	errorHandler(Err)

	return requestBody
}

func getResponseChannelID(dg *discordgo.Session, userID string) string {
	userChannel, Err := dg.UserChannelCreate(userID)
	errorHandler(Err)

	responseChannelID := userChannel.ID

	return responseChannelID
}

func processMessage(requestBody []byte) map[string]interface{} {
	response, Err := http.Post(os.Getenv("GUTHO_MAIN_URL"), "application/json", bytes.NewBuffer(requestBody))
	errorHandler(Err)

	defer response.Body.Close()

	responseBody, Err := ioutil.ReadAll(response.Body)
	errorHandler(Err)

	var responseContent map[string]interface{}
	json.Unmarshal([]byte(responseBody), &responseContent)

	return responseContent
}
