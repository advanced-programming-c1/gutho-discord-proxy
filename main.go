package main

import (
	"github.com/bwmarrin/discordgo"
)

// Dg is the Discord session
var Dg *discordgo.Session

// BotToken is the token of Discord Bot
var BotToken string

// BotID is the ID of the Discord Bot
var BotID string

var BotUser *discordgo.User

// Err is the Errors
var Err error

func main() {
	initialize()

	<-make(chan struct{})
	return
}
