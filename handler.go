package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/bwmarrin/discordgo"
)

func messageHandler(dg *discordgo.Session, message *discordgo.MessageCreate) {
	if message.Author.ID == BotID {
		return
	} else if string(message.Content) == "ping" {
		_, _ = dg.ChannelMessageSend(message.ChannelID, "pong "+message.Author.Username)
	} else if string(message.Content) == "!help" {
		_, _ = dg.ChannelMessageSend(message.ChannelID, helpMessage())
	} else {
		requestBody := generateRequestBody(message)
		responseChannelID := message.ChannelID

		response := processMessage(requestBody)

		responseType := fmt.Sprintf("%v", response["responseType"])
		responseMessage := fmt.Sprintf("%v", response["message"])

		if responseType == "PM" {
			responseUserID := fmt.Sprintf("%v", response["userID"])
			responseChannelID = getResponseChannelID(dg, responseUserID)
		}

		if responseType == "GM2" {
			responseChannelID = fmt.Sprintf("%v", response["userID"])
		}

		if responseMessage != "" {
			_, _ = dg.ChannelMessageSend(responseChannelID, responseMessage)
		}
	}
}

func messageSendRequestHandler(dg *discordgo.Session) {
	http.HandleFunc("/api/v1/sendmessage", func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()

		requestBody, Err := ioutil.ReadAll(r.Body)
		errorHandler(Err)

		var requestContent map[string]interface{}
		json.Unmarshal([]byte(requestBody), &requestContent)

		fmt.Fprintf(w, "Message successfully sent to %v", requestContent["channelID"])

		channelID := fmt.Sprintf("%v", requestContent["channelID"])
		messageContent := fmt.Sprintf("%v", requestContent["message"])

		_, Err = dg.ChannelMessageSend(channelID, messageContent)
		errorHandler(Err)
	})

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))
}

func errorHandler(err error) {
	if err != nil {
		panic(err)
	}
}

func helpMessage() string {
	helpString := `
╭━━━╮╱╱╭╮╭╮
┃╭━╮┃╱╭╯╰┫┃
┃┃╱╰╋╮┣╮╭┫╰━┳━━╮
┃┃╭━┫┃┃┃┃┃╭╮┃╭╮┃
┃╰┻━┃╰╯┃╰┫┃┃┃╰╯┃
╰━━━┻━━┻━┻╯╰┻━━╯
Guess the Word

Use the command "!create" to create a new game.
Your friends can then join the game by using the command "!join".
Use the command "!start" to start the game and enjoy!
	`

	return helpString
}
