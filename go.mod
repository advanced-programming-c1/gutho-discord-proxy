module gitlab.com/advanced-programming-c1/gutho-discord-proxy

// +heroku goVersion go1.14
go 1.14

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/gin-gonic/gin v1.7.1
	github.com/joho/godotenv v1.3.0
)
