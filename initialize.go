package main

import (
	"fmt"
	"os"

	"github.com/bwmarrin/discordgo"
)

func initialize() {
	BotToken = os.Getenv("GUTHO_TOKEN")

	Dg, Err = discordgo.New("Bot " + BotToken)
	errorHandler(Err)

	BotUser, Err = Dg.User("@me")
	errorHandler(Err)

	BotID = BotUser.ID

	Dg.AddHandler(messageHandler)

	Err = Dg.Open()

	errorHandler(Err)

	Dg.UpdateGameStatus(1, "!help")

	fmt.Println("Bot is running")

	go messageSendRequestHandler(Dg)

	fmt.Println("Message send service is running")

	fmt.Println("Initialization is complete")
}
