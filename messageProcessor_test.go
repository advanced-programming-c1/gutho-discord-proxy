package main

import (
	"testing"

	"github.com/bwmarrin/discordgo"
)

func TestGenerateRequestBody(t *testing.T) {
	message := new(discordgo.MessageCreate)
	message.Message = new(discordgo.Message)
	message.Content = "test content"
	message.ChannelID = "000"
	message.Author = new(discordgo.User)
	message.Author.ID = "111"
	if string(generateRequestBody(message)) != "{\"channelID\":\"000\",\"message\":\"test content\",\"userID\":\"111\"}" {
		t.Errorf("Request body format doesn't match")
	}
}
