FROM golang:1.15-alpine AS builder

WORKDIR /app

COPY . .

RUN ["go", "build"]

FROM golang:1.15-alpine

COPY --from=builder /app/gutho-discord-proxy app

ARG GUTHO_TOKEN

ENV GUTHO_TOKEN=$GUTHO_TOKEN

CMD ["./app"]